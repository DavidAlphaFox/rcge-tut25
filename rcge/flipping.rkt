#lang racket/base

(provide flip-frame!
         last-frame-duration
         last-frame-overrun?
         fullscreen!
         frame-size-callback)

(require "drivers.rkt"
         "time.rkt"
         "draw.rkt"
         "driver-run.rkt"
         (only-in racket/fixnum make-fxvector)
         racket/require
         (for-syntax racket/base)
         (filtered-in
          (λ (name)
            (regexp-replace #rx"unsafe-" name ""))
          racket/unsafe/ops))

(define (flip-frame!)
  (define drv (current-rcge-driver))
  ((rcge-driver-flip-pre-proc drv))

  ; Handle timing, driver independent
  (define tctx (current-time-ctx))
  (define current-ts (time-ctx-last-timestamp tctx))
  (define duration (time-ctx-expected-duration tctx))
  (define this-ts (current-inexact-milliseconds))
  (define last-duration (- this-ts current-ts))
  (cond ((> (time-ctx-fps tctx) 0)
         (set-time-ctx-last-duration! tctx last-duration)
         (cond ((< last-duration duration)
                (define to-sleep (/ (- duration last-duration) 1000))
                (sleep to-sleep)
                (set-time-ctx-effective-duration! tctx duration)
                (set-time-ctx-overrun! tctx #f)
                (set-time-ctx-last-timestamp! tctx (+ current-ts duration)))
               (else
                (set-time-ctx-effective-duration! tctx last-duration)
                (set-time-ctx-overrun! tctx #t)
                (set-time-ctx-last-timestamp! tctx this-ts))))
        (else
         ; continuous
         (set-time-ctx-last-duration! tctx last-duration)
         (set-time-ctx-effective-duration! tctx last-duration)
         (set-time-ctx-overrun! tctx #f)
         (set-time-ctx-last-timestamp! tctx this-ts)))

  ; size changes - definitely driver dependent
  (define size-changed
    ((rcge-driver-flip-do-proc drv)))
  
  ; size change callbacks - should go to draw context, not backend! (and should be driver-independent)
  (when size-changed
    (define ctx (current-draw-ctx))
    (define-values (new-width new-height) (apply values size-changed))
    ; driver-independent:
    (set-draw-ctx-width! ctx new-width)
    (set-draw-ctx-height! ctx new-height)
    (set-draw-ctx-framebuffer! ctx (make-fxvector (fx* new-width new-height)))
    (for (((name callback) (draw-ctx-size-callbacks (current-draw-ctx))))
      (callback)))
  (void))

(define (last-frame-duration)
  (time-ctx-effective-duration (current-time-ctx)))

(define (last-frame-overrun?)
  (time-ctx-overrun (current-time-ctx)))

(define (fullscreen! (f 'toggle))
  (define drv (current-rcge-driver))
  (define fullscreen-proc (rcge-driver-fullscreen-proc drv))
  (when fullscreen-proc
    (fullscreen-proc f)))

(define (frame-size-callback name proc)
  (define ctx (current-draw-ctx))
  (define size-callbacks (draw-ctx-size-callbacks ctx))
  (if proc
      (hash-set! size-callbacks name proc)
      (hash-remove! size-callbacks name)))

