#lang racket/base

(provide mat4-mov3d
         mat4-rot/x
         mat4-rot/y
         mat4-rot/z
         mat4-rot3d)

(require racket/require
         (for-syntax racket/base)
         (filtered-in
          (λ (name)
            (regexp-replace #rx"unsafe-" name ""))
          racket/unsafe/ops)
         "algebra.rkt")

(define (mat4-mov3d x y z)
  (define R (mat4-ident))
  (flvector-set! R 3 x)
  (flvector-set! R 7 y)
  (flvector-set! R 11 z)
  R)

(define (mat4-rot/z alpha)
  (define R (make-mat4))
  (flvector-set! R 0 (flcos alpha))
  (flvector-set! R 1 (fl- (flsin alpha)))
  (flvector-set! R 4 (flsin alpha))
  (flvector-set! R 5 (flcos alpha))
  (flvector-set! R 10 1.0)
  (flvector-set! R 15 1.0)
  R)

(define (mat4-rot/y alpha)
  (define R (make-mat4))
  (flvector-set! R 0 (flcos alpha))
  (flvector-set! R 2 (fl- (flsin alpha)))
  (flvector-set! R 5 1.0)
  (flvector-set! R 8 (flsin alpha))
  (flvector-set! R 10 (flcos alpha))
  (flvector-set! R 15 1.0)
  R)

(define (mat4-rot/x alpha)
  (define R (make-mat4))
  (flvector-set! R 0 1.0)
  (flvector-set! R 5 (flcos alpha))
  (flvector-set! R 6 (fl- (flsin alpha)))
  (flvector-set! R 9 (flsin alpha))
  (flvector-set! R 10 (flcos alpha))
  (flvector-set! R 15 1.0)
  R)

(define (mat4-rot3d alpha beta gamma)
  (mat4*mat4
   (mat4-rot/z gamma)
   (mat4*mat4
    (mat4-rot/x beta)
    (mat4-rot/y gamma))))
